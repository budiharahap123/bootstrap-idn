<hr size="1">
<footer class="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-4 col-xs-4 col-md-offset-0 col-sm-offset-4 col-xs-offset-4">
				<a href="https://www.facebook.com/indonetwork.co.id/" target="_blank"><ico class="facebook"></ico></a>
				<a href="https://twitter.com/IndonetworkCoId" target="_blank"><ico class="twitter"></ico></a>
				<a href="https://www.youtube.com/channel/UCjwsIUD8cqrgaxQDoV1jDXQ" target="_blank"><ico class="youtube"></ico></a>
				<a href="https://www.linkedin.com/company/indonetwork-co-id?trk=biz-companies-cym" target="_blank"><ico class="linkedin"></ico></a>
			</div>
			<div class="col-md-9 col-sm-12 col-xs-12 more-foot">
				<div class="row">
					<div class="col-md-3 col-sm-4 col-xs-4">
						<h3>Indonetwork</h3>
						<ul>
							<li><a href="./?page=static&amp;view=kategori semua">Telusuri Kategori</a></li>
							<li><a href="">Tentang Kami</a></li>
							<li><a href="">Syarat &amp; Ketentuan</a></li>
							<li><a href="">Kebijakan &amp; Privasi</a></li>
							<li><a href="">Hubungi Kami</a></li>
							<li><a href="http://blog.indonetwork.co.id" target="_Blank" rel="unfollow" class="footblog">Blog</a></li>
						</ul>
					</div>
					<div class="col-md-3 col-sm-4 col-xs-4">
						<h3>Pembeli</h3>
						<ul>
							<li><a href="./?page=static&amp;view=how to register">Cara Daftar</a></li>
							<li><a href="./?page=static&amp;view=how to buy">Cara Beli Produk</a></li>
						</ul>
					</div>
					<div class="col-md-3 col-sm-4 col-xs-4">
						<h3>Penjual</h3>
						<ul>
							<li><a href="./?page=static&amp;view=how to register">Cara Daftar</a></li>
							<li><a href="./?page=static&amp;view=how to sell">Cara Jual Produk</a></li>
							<li><a href="./?page=static&amp;view=how to upgrade">Cara Upgrade Prioritas</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="footer-city">
			<hr size="1">
			<ul>
				<li><a href="http://www.indonetwork.co.id/aceh">Aceh</a></li>
				<li><a href="http://www.indonetwork.co.id/bali">Bali</a></li>
				<li><a href="http://www.indonetwork.co.id/banten">Banten</a></li>
				<li><a href="http://www.indonetwork.co.id/bengkulu">Bengkulu</a></li>
				<li><a href="http://www.indonetwork.co.id/gorontalo">Gorontalo</a></li>
				<li><a href="http://www.indonetwork.co.id/dki-jakarta">DKI Jakarta</a></li>
				<li><a href="http://www.indonetwork.co.id/jambi">Jambi</a></li>
				<li><a href="http://www.indonetwork.co.id/jawa-barat">Jawa Barat</a></li>
				<li><a href="http://www.indonetwork.co.id/jawa-tengah">Jawa Tengah</a></li>
				<li><a href="http://www.indonetwork.co.id/jawa-timur">Jawa Timur</a></li>
				<li><a href="http://www.indonetwork.co.id/kalimantan-barat">Kalimantan Barat</a></li>
				<li><a href="http://www.indonetwork.co.id/kalimantan-selatan">Kalimantan Selatan</a></li>
				<li><a href="http://www.indonetwork.co.id/kalimantan-tengah">Kalimantan Tengah</a></li>
				<li><a href="http://www.indonetwork.co.id/kalimantan-timur">Kalimantan Timur</a></li>
				<li><a href="http://www.indonetwork.co.id/kalimantan-utara">Kalimantan Utara</a></li>
				<li><a href="http://www.indonetwork.co.id/kepulauan-bangka-belitung">Kepulauan Bangka Belitung</a></li>
				<li><a href="http://www.indonetwork.co.id/kepulauan-riau">Kepulauan Riau</a></li>
				<li><a href="http://www.indonetwork.co.id/lampung">Lampung</a></li>
				<li><a href="http://www.indonetwork.co.id/kepulauan-maluku">Kepulauan Maluku</a></li>
				<li><a href="http://www.indonetwork.co.id/maluku-utara">Maluku Utara</a></li>
				<li><a href="http://www.indonetwork.co.id/nusa-tenggara-barat">Nusa Tenggara Barat</a></li>
				<li><a href="http://www.indonetwork.co.id/nusa-tenggara-timur">Nusa Tenggara Timur</a></li>
				<li><a href="http://www.indonetwork.co.id/papua">Papua</a></li>
				<li><a href="http://www.indonetwork.co.id/papua-barat">Papua Barat</a></li>
				<li><a href="http://www.indonetwork.co.id/riau">Riau</a></li>
				<li><a href="http://www.indonetwork.co.id/sulawesi-barat">Sulawesi Barat</a></li>
				<li><a href="http://www.indonetwork.co.id/sulawesi-selatan">Sulawesi Selatan</a></li>
				<li><a href="http://www.indonetwork.co.id/sulawesi-tengah">Sulawesi Tengah</a></li>
				<li><a href="http://www.indonetwork.co.id/sulawesi-tenggara">Sulawesi Tenggara</a></li>
				<li><a href="http://www.indonetwork.co.id/sulawesi-utara">Sulawesi Utara</a></li>
				<li><a href="http://www.indonetwork.co.id/sumatera-barat">Sumatera Barat</a></li>
				<li><a href="http://www.indonetwork.co.id/sumatera-selatan">Sumatera Selatan</a></li>
				<li><a href="http://www.indonetwork.co.id/sumatera-utara">Sumatera Utara</a></li>
				<li><a href="http://www.indonetwork.co.id/daerah-istimewa-yogyakarta">Daerah Istimewa Yogyakarta</a></li>
			</ul>
			<hr size="1">
		</div>
		<center>
			&copy; 2001-2016 <strong>Indonetwork.co.id</strong>. Hak cipta dilindungi undang-undang. •  e-mail <a href="mailto:support@indonetwork.co.id">support@indonetwork.co.id</a>
		</center>
	</div>
</footer>
<script src="../bootstrap-sass/assets/javascripts/custom.js"></script>
<script src="../bootstrap-sass/assets/javascripts/executor.js"></script>
</body>
</html>