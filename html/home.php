<?php include ("head.php");?>
<?php include ("head-home.php");?>
<div class="container-fluid">
	<div class="wrap">
		<div class="kategoribox con">
			<div class="row">
				<ul class="col-md-12 col-sm-12 col-xs-12">
			        <li><a href="/teknologi-dan-telekomunikasi"><ico class="teknologi-dan-telekomunikasi"></ico>Teknologi &amp; Telekomunikasi</a></li>
			        <li><a href="/peralatan-kantor"><ico class="peralatan-kantor"></ico>Peralatan Kantor</a></li>
			        <li><a href="/elektronik-dan-peralatan-rumah-tangga"><ico class="elektronik-dan-peralatan-rumah-tangga"></ico>Elektronik &amp; Peralatan Rumah Tangga</a></li>
			        <li><a href="/peralatan-elektronik"><ico class="peralatan-elektronik"></ico>Peralatan Elektronik</a></li>
			        <li><a href="/kesehatan-dan-kecantikan"><ico class="kesehatan-dan-kecantikan"></ico>Kesehatan &amp; Kecantikan</a></li>
			        <li><a href="/peralatan-industri"><ico class="peralatan-industri"></ico>Peralatan Industri</a></li>
			        <li><a href="/grocery"><ico class="grocery"></ico>Grocery</a></li>
			        <li><a href="/otomotif-dan-transportasi"><ico class="otomotif-dan-transportasi"></ico>Otomotif &amp; Transportasi</a></li>
			        <li><a href="/media-musik-dan-buku"><ico class="media-musik-dan-buku"></ico>Media, Musik &amp; Buku</a></li>
			        <li><a href="/peralatan-olahraga-dan-outdoor"><ico class="peralatan-olahraga-dan-outdoor"></ico>Peralatan Olahraga &amp; Outdoor</a></li>
			        <li><a href="/mainan-dan-bayi"><ico class="mainan-dan-bayi"></ico>Mainan &amp; Bayi</a></li>
			        <li><a href="/fashion-tekstil-dan-aksesoris"><ico class="fashion-tekstil-dan-aksesoris"></ico>Fashion, Tekstil, &amp; Aksesoris</a></li>
			        <li><a href="/pertanian-dan-produk-pangan"><ico class="pertanian-dan-produk-pangan"></ico>Pertanian &amp; Produk Pangan</a></li>
			        <li><a href="/voucher-dan-layanan"><ico class="voucher-dan-layanan"></ico>Voucher &amp; Layanan</a></li>
			        <li><a href="/kerajinan-dan-souvenir"><ico class="kerajinan-dan-souvenir"></ico>Kerajinan &amp; Souvenir</a></li>
			        <li><a href="/metalurgi-mineral-dan-kimia"><ico class="metalurgi-mineral-dan-kimia"></ico>Metalurgi, Mineral &amp; Kimia</a></li>
			    </ul>
			</div>
		</div>
	</div>
</div>
<div class="featurebar">
    <div class="wrap">
    	<div class="container-fluid">
    		<div class="row">
		        <ul class="col-md-12 col-sm-12 col-xs-12">
		            <li><a href="http://www.indonetwork.co.id/categories"><ico class="kategori"></ico>Telusuri Kategori</a></li>
		            <li><a href="#new-list"><ico class="quotation"></ico>Produk Terbaru</a></li>
		            <li><a href="#situs-web"><ico class="web"></ico>Panduan Pembuatan Konten</a></li>
		            <li><a href="#transaksi-aman"><ico class="aman"></ico>Data Aman &amp; Terjamin</a></li>
		        </ul>
		    </div>
		</div>
    </div>
</div>
<div class="container-fluid">
	<div class="wrap">
		<div id="myCarousel" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
		      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		      <li data-target="#myCarousel" data-slide-to="1"></li>
		      <li data-target="#myCarousel" data-slide-to="2"></li>
		      <li data-target="#myCarousel" data-slide-to="3"></li>
		    </ol>
		    <div class="carousel-inner" role="listbox">
		    	<div class="item active">
		    		<ul class="row">
		    			<li class="col-md-6 col-sm-12 col-xs-12">
		    				<a href="#" target="_Blank">
               					<img src="../bootstrap-sass/assets/images/banners/lucky-draw.png" alt="direktori b2b terbesar di indonesia" class="img-responsive">
            				</a>
            			</li>
		    			<li class="col-md-6 col-sm-12 col-xs-12">
		    				<a href="#" target="_Blank">
               					<img src="../bootstrap-sass/assets/images/banners/event-01.jpg" alt="direktori b2b terbesar di indonesia" class="img-responsive">
            				</a>
		    			</li>
		    		</ul>
		    	</div>
		    	<div class="item">
		    		<ul class="row">
		    			<li class="col-md-6 col-sm-12 col-xs-12">
		    				<a href="#" target="_Blank">
               					<img src="../bootstrap-sass/assets/images/banners/event-03.jpg" alt="direktori b2b terbesar di indonesia" class="img-responsive">
            				</a>
            			</li>
		    			<li class="col-md-6 col-sm-12 col-xs-12">
		    				<a href="#" target="_Blank">
               					<img src="../bootstrap-sass/assets/images/banners/event-04.jpg" alt="direktori b2b terbesar di indonesia" class="img-responsive">
            				</a>
		    			</li>
		    		</ul>
		    	</div>
		    	<div class="item">
		    		<ul class="row">
		    			<li class="col-md-6 col-sm-12 col-xs-12">
		    				<a href="#" target="_Blank">
               					<img src="../bootstrap-sass/assets/images/banners/lucky-draw.png" alt="direktori b2b terbesar di indonesia" class="img-responsive">
            				</a>
            			</li>
		    			<li class="col-md-6 col-sm-12 col-xs-12">
		    				<a href="#" target="_Blank">
               					<img src="../bootstrap-sass/assets/images/banners/event-01.jpg" alt="direktori b2b terbesar di indonesia" class="img-responsive">
            				</a>
		    			</li>
		    		</ul>
		    	</div>
		    	<div class="item">
		    		<ul class="row">
		    			<li class="col-md-6 col-sm-12 col-xs-12">
		    				<a href="#" target="_Blank">
               					<img src="../bootstrap-sass/assets/images/banners/event-03.jpg" alt="direktori b2b terbesar di indonesia" class="img-responsive">
            				</a>
            			</li>
		    			<li class="col-md-6 col-sm-12 col-xs-12">
		    				<a href="#" target="_Blank">
               					<img src="../bootstrap-sass/assets/images/banners/event-04.jpg" alt="direktori b2b terbesar di indonesia" class="img-responsive">
            				</a>
		    			</li>
		    		</ul>
		    	</div>
		    </div>
		</div>
		<div class="new-list" id="new-list">
			<div class="row">
				<div class="col-md-6 col-sm-12 col-xs-12">
					<div class="new-product" id="prod_gallery_1">
						<h3 class="title">Katalog Terbaru</h3>
						<div class="thumb-gallery con">
							<div class="col-md-12 col-sm-8 col-xs-12 col-md-offset-0 col-sm-offset-2">
								<div class="col-md-7 col-sm-7 col-xs-7 bigimg">	
									<div class="imgbox">
										<div class="imgchange_container">
											<img src="../bootstrap-sass/assets/images/produk/foto1.jpg" alt="nama banner" class="img-responsive">
										</div>
									</div>
								</div>
								<div class="col-md-5 col-sm-5 col-xs-5 smallimg">
									<ul class="row">
										<li class="col-md-6 col-sm-6 col-xs-6">
											<div class="selected trigger" data-target="../bootstrap-sass/assets/images/produk/foto1.jpg" alt="nama banner" data-prodinfo="prodinfo1">
												<div class="imgbox">
													<img src="../bootstrap-sass/assets/images/produk/foto1.jpg" alt="nama banner" class="img-responsive">
												</div>
											</div>
										</li>
										<li class="col-md-6 col-sm-6 col-xs-6">
											<div class="trigger" data-target="../bootstrap-sass/assets/images/produk/foto2.jpg" alt="nama banner" data-prodinfo="prodinfo2">
												<div class="imgbox">
													<img src="../bootstrap-sass/assets/images/produk/foto2.jpg" alt="nama banner" class="img-responsive">
												</div>
											</div>
										</li>
										<li class="col-md-6 col-sm-6 col-xs-6">
											<div class="trigger" data-target="../bootstrap-sass/assets/images/produk/foto3.jpg" alt="nama banner" data-prodinfo="prodinfo3">
												<div class="imgbox">
													<img src="../bootstrap-sass/assets/images/produk/foto3.jpg" alt="nama banner" class="img-responsive">
												</div>
											</div>
										</li>
										<li class="col-md-6 col-sm-6 col-xs-6">
											<div class="trigger" data-target="../bootstrap-sass/assets/images/produk/foto4.jpg" alt="nama banner" data-prodinfo="prodinfo4">
												<div class="imgbox">
													<img src="../bootstrap-sass/assets/images/produk/foto4.jpg" alt="nama banner" class="img-responsive">
												</div>
											</div>
										</li>
										<li class="col-md-6 col-sm-6 col-xs-6">
											<div class="trigger" data-target="../bootstrap-sass/assets/images/produk/foto5.jpg" alt="nama banner" data-prodinfo="prodinfo5">
												<div class="imgbox">
													<img src="../bootstrap-sass/assets/images/produk/foto5.jpg" alt="nama banner" class="img-responsive">
												</div>
											</div>
										</li>
										<li class="col-md-6 col-sm-6 col-xs-6">
											<div class="trigger" data-target="../bootstrap-sass/assets/images/produk/foto7.jpg" alt="nama banner" data-prodinfo="prodinfo6">
												<div class="imgbox">
													<img src="../bootstrap-sass/assets/images/produk/foto7.jpg" alt="nama banner" class="img-responsive">
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12">
							<ul class="row product-info">
								<li class="col-md-12 col-sm-12 col-xs-12 product-info-list prodinfo1" style="display:block;">
									<div class="row">
										<div class="col-md-8 col-sm-8 col-xs-12">
											<h2 class="data_title">Judul 1Aoccdrnig to rseearch at an Elingsh uinervtisy, it deosn't have any new try</h2>
											<div class="row hargabox">
												<div class="col-md-6 col-sm-6  col-xs-12">
													<span class="price">199.000.000.000</span>
												</div>
												<div class="col-md-2 col-sm-6 col-xs-6 minorder">
													<span>min. order</span>
													<strong>5</strong> Buah
												</div>
												<div class="col-md-4 col-sm-6 col-xs-6 stock">
													<span>tayang hingga</span>
													<strong>28/04/2016</strong>
												</div>
											</div>
											<p class="more">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, q...</p>
										</div>
										<div class="col-md-4 col-sm-4 col-xs-12 buythis">
											<a href="" class="button">Beli Produk Ini</a>
										</div>
									</div>
								</li>
								<li class="col-md-12 col-sm-12 col-xs-12 product-info-list prodinfo2">
									<div class="row">
										<div class="col-md-8 col-sm-8 col-xs-12">
											<h2 class="data_title">Judul 2 Aoccdrnig to rseearch at an Elingsh uinervtisy, it deosn't have any new try</h2>
											<div class="row hargabox">
												<div class="col-md-6 col-sm-6  col-xs-12">
													<span class="price">199.000.000.000</span>
												</div>
												<div class="col-md-2 col-sm-6 col-xs-6 minorder">
													<span>min. order</span>
													<strong>5</strong> Buah
												</div>
												<div class="col-md-4 col-sm-6 col-xs-6 stock">
													<span>tayang hingga</span>
													<strong>28/04/2016</strong>
												</div>
											</div>
											<p class="more">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, q...</p>
										</div>
										<div class="col-md-4 col-sm-4 col-xs-12 buythis">
											<a href="" class="button">Beli Produk Ini</a>
										</div>
									</div>
								</li>
								<li class="col-md-12 col-sm-12 col-xs-12 product-info-list prodinfo3">
									<div class="row">
										<div class="col-md-8 col-sm-8 col-xs-12">
											<h2 class="data_title">Judul 3 Aoccdrnig to rseearch at an Elingsh uinervtisy, it deosn't have any new try</h2>
											<div class="row hargabox">
												<div class="col-md-6 col-sm-6  col-xs-12">
													<span class="price">199.000.000.000</span>
												</div>
												<div class="col-md-2 col-sm-6 col-xs-6 minorder">
													<span>min. order</span>
													<strong>5</strong> Buah
												</div>
												<div class="col-md-4 col-sm-6 col-xs-6 stock">
													<span>tayang hingga</span>
													<strong>28/04/2016</strong>
												</div>
											</div>
											<p class="more">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, q...</p>
										</div>
										<div class="col-md-4 col-sm-4 col-xs-12 buythis">
											<a href="" class="button">Beli Produk Ini</a>
										</div>
									</div>
								</li>
								<li class="col-md-12 col-sm-12 col-xs-12 product-info-list prodinfo4">
									<div class="row">
										<div class="col-md-8 col-sm-8 col-xs-12">
											<h2 class="data_title">Judul 4 Aoccdrnig to rseearch at an Elingsh uinervtisy, it deosn't have any new try</h2>
											<div class="row hargabox">
												<div class="col-md-6 col-sm-6  col-xs-12">
													<span class="price">199.000.000.000</span>
												</div>
												<div class="col-md-2 col-sm-6 col-xs-6 minorder">
													<span>min. order</span>
													<strong>5</strong> Buah
												</div>
												<div class="col-md-4 col-sm-6 col-xs-6 stock">
													<span>tayang hingga</span>
													<strong>28/04/2016</strong>
												</div>
											</div>
											<p class="more">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, q...</p>
										</div>
										<div class="col-md-4 col-sm-4 col-xs-12 buythis">
											<a href="" class="button">Beli Produk Ini</a>
										</div>
									</div>
								</li>
								<li class="col-md-12 col-sm-12 col-xs-12 product-info-list prodinfo5">
									<div class="row">
										<div class="col-md-8 col-sm-8 col-xs-12">
											<h2 class="data_title">Judul 5 Aoccdrnig to rseearch at an Elingsh uinervtisy, it deosn't have any new try</h2>
											<div class="row hargabox">
												<div class="col-md-6 col-sm-6  col-xs-12">
													<span class="price">199.000.000.000</span>
												</div>
												<div class="col-md-2 col-sm-6 col-xs-6 minorder">
													<span>min. order</span>
													<strong>5</strong> Buah
												</div>
												<div class="col-md-4 col-sm-6 col-xs-6 stock">
													<span>tayang hingga</span>
													<strong>28/04/2016</strong>
												</div>
											</div>
											<p class="more">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, q...</p>
										</div>
										<div class="col-md-4 col-sm-4 col-xs-12 buythis">
											<a href="" class="button">Beli Produk Ini</a>
										</div>
									</div>
								</li>
								<li class="col-md-12 col-sm-12 col-xs-12 product-info-list prodinfo6">
									<div class="row">
										<div class="col-md-8 col-sm-8 col-xs-12">
											<h2 class="data_title">Judul 6 Aoccdrnig to rseearch at an Elingsh uinervtisy, it deosn't have any new try</h2>
											<div class="row hargabox">
												<div class="col-md-6 col-sm-6  col-xs-12">
													<span class="price">199.000.000.000</span>
												</div>
												<div class="col-md-2 col-sm-6 col-xs-6 minorder">
													<span>min. order</span>
													<strong>5</strong> Buah
												</div>
												<div class="col-md-4 col-sm-6 col-xs-6 stock">
													<span>tayang hingga</span>
													<strong>28/04/2016</strong>
												</div>
											</div>
											<p class="more">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, q...</p>
										</div>
										<div class="col-md-4 col-sm-4 col-xs-12 buythis">
											<a href="" class="button">Beli Produk Ini</a>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-sm-12 col-xs-12">
					<div class="new-rfq">
						<h3 class="title">Permintaan Terbaru</h3>
						<ul>
							<li class="col-md-12 col-sm-12 col-xs-12">
								<div class="col-md-7 col-sm-7 col-xs-7">
									<div class="new-rfq-l">
										<h4><a href="#">Aoccdrnig to rseearch at an Elingsh uinervtisy, it deosn'tad a</a></h4>
										<div class="membership"><img alt="membership anggota prioritas" src="../bootstrap-sass/assets/images/award.png"><h2 class="sn-limit">PT. Cakrawala Mendung Payung Berkembang Cahaya</h2></div>
										<small>Jakarta Selatan • DKI Jakarta • ID</small>
										<p class="more">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
									</div>
								</div>
								<div class="col-md-5 col-sm-5 col-xs-5">
									<div class="new-rfq-r">
										<span class="price">199.000.000.000</span>
										<div class="rfq-inf">
											<div class="row">
												<div class="col-md-6 col-sm-6 col-xs-6"><span>jumlah penawaran</span> <strong>13</strong> vendor</div>
												<div class="col-md-6 col-sm-6 col-xs-6"><span>tayang hingga</span> <strong>28/04/2016</strong></div>
											</div>
										</div>
										<a href=""class="button">Detail Permintaan</a>
									</div>
								</div>
							</li>
							<li class="col-md-12 col-sm-12 col-xs-12">
								<div class="col-md-7 col-sm-7 col-xs-7">
									<div class="new-rfq-l">
										<h4><a href="#">Aoccdrnig to rseearch at an Elingsh uinervtisy, it deosn'tad a</a></h4>
										<div class="membership"><img alt="membership anggota prioritas" src="../bootstrap-sass/assets/images/award.png"><h2 class="sn-limit">PT. Cakrawala Mendung Payung Berkembang Cahaya</h2></div>
										<small>Jakarta Selatan • DKI Jakarta • ID</small>
										<p class="more">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
									</div>
								</div>
								<div class="col-md-5 col-sm-5 col-xs-5">
									<div class="new-rfq-r">
										<span class="price">199.000.000.000</span>
										<div class="rfq-inf">
											<div class="row">
												<div class="col-md-6 col-sm-6 col-xs-6"><span>jumlah penawaran</span> <strong>13</strong> vendor</div>
												<div class="col-md-6 col-sm-6 col-xs-6"><span>tayang hingga</span> <strong>28/04/2016</strong></div>
											</div>
										</div>
										<a href=""class="button">Detail Permintaan</a>
									</div>
								</div>
							</li>
							<li class="col-md-12 col-sm-12 col-xs-12">
								<div class="col-md-7 col-sm-7 col-xs-7">
									<div class="new-rfq-l">
										<h4><a href="#">Aoccdrnig to rseearch at an Elingsh uinervtisy, it deosn'tad a</a></h4>
										<div class="membership"><img alt="membership anggota prioritas" src="../bootstrap-sass/assets/images/award.png"><h2 class="sn-limit">PT. Cakrawala Mendung Payung Berkembang Cahaya</h2></div>
										<small>Jakarta Selatan • DKI Jakarta • ID</small>
										<p class="more">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
									</div>
								</div>
								<div class="col-md-5 col-sm-5 col-xs-5">
									<div class="new-rfq-r">
										<span class="price">199.000.000.000</span>
										<div class="rfq-inf">
											<div class="row">
												<div class="col-md-6 col-sm-6 col-xs-6"><span>jumlah penawaran</span> <strong>13</strong> vendor</div>
												<div class="col-md-6 col-sm-6 col-xs-6"><span>tayang hingga</span> <strong>28/04/2016</strong></div>
											</div>
										</div>
										<a href=""class="button">Detail Permintaan</a>
									</div>
								</div>
							</li>
						</ul>
						<a class="other-newreq" href="./?page=listing-request">Lihat Permintaan Lain</a>
					</div>
				</div>
			</div>
		</div>
		<div class="home-tittle">
			<hr size="1">
			<h2>Direktori Bisnis Online dan UKM Terbesar di Indonesia</h2>
			<hr size="1">
		</div>
		<div class="desc-ind">
			<div class="row" id="situs-web">
				<div class="col-md-6 col-sm-6 col-xs-6 col-md-offset-0 col-sm-offset-3 col-xs-offset-3"><img src="../bootstrap-sass/assets/images/feature-web-01.jpg" alt="nama banner" class="img-responsive"></div>
				<div class="col-md-6 col-sm-12 col-xs-12 desc-ind-capt">
					<div class="title">
						<ico class="web"></ico>
						Panduan Pembuatan Konten
					</div>
					<p>Menjaga kualitas konten sangatlah penting untuk meningkatkan kepercayaan dan memberikan informasi yang jelas pada pengguna lain terhadap produk yang akan Anda tawarkan. Hal ini akan sangat terasa manfaatnya terutama ketika Anda berperan sebagai penjual, dengan memiliki kualitas konten yang baik tentunya akan mempercepat penjualan Anda, dan dapat menjadikan Anda sebagai salah satu Recommended Seller di Indonetwork.co.id!</p>
					<a href="" class="button">Baca Selengkapnya &raquo;</a>
				</div>
			</div>
			<hr size="1">
			<div class="row secure1" id="transaksi-aman">
				<div class="col-md-6 col-sm-12 desc-ind-capt">
					<div class="title">
						<ico class="aman"></ico>
						Data Aman &amp; Terjamin
					</div>
					<p>Semua data mengenai produk yang Anda Iklankan, serta informasi perusahaan yang Anda tampilkan akan tersimpan dan terjamin keamanannya. Sehingga Anda tidak perlu ragu lagi untuk mempercayai kami sebagai sarana untuk meningkatkan penjualan produk Anda.</p>
					<a href="" class="button">Baca Selengkapnya &raquo;</a>
				</div>
				<div class="col-md-6 col-sm-12"><img src="../bootstrap-sass/assets/images/feature-escrow.jpg" alt="nama banner" class="img-responsive"></div>
			</div>
			<div class="row secure2" id="transaksi-aman">
				<div class="col-md-6 col-sm-6 col-xs-6 col-md-offset-0 col-sm-offset-3 col-xs-offset-3"><img src="../bootstrap-sass/assets/images/feature-escrow.jpg" alt="nama banner" class="img-responsive"></div>
				<div class="col-md-6 col-sm-12 col-xs-12 desc-ind-capt">
					<div class="title">
						<ico class="aman"></ico>
						Data Aman &amp; Terjamin
					</div>
					<p>Semua data mengenai produk yang Anda Iklankan, serta informasi perusahaan yang Anda tampilkan akan tersimpan dan terjamin keamanannya. Sehingga Anda tidak perlu ragu lagi untuk mempercayai kami sebagai sarana untuk meningkatkan penjualan produk Anda.</p>
					<a href="" class="button">Baca Selengkapnya &raquo;</a>
				</div>
			</div>
			<hr size="1">
		</div>
		<div class="seo-ind">
			<p>Indonetwork.co.id merupakan pusat promosi perdagangan dan direktori bisnis B2B (business-to-business) serta B2C (business-to-customer) terlengkap dan terbesar di Indonesia. Prioritas utama kami adalah memperlebar peluang usaha para UMKM (Usaha Mikro Kecil dan Menengah) untuk menjadi penyedia barang beragam perusahaan, individual, pelaku bisnis, dan industri yang mencakup seluruh wilayah di Indonesia. Untuk para pembeli, adanya permintaan dan kebutuhaan perorangan dan juga supply chain perusahaan membuat kami mengerti akan kebutuhan suatu direktori bisnis online yang terintegrasi, efisien, serta mempermudah pencarian barang dan jasa yang diperjualbelikan. Maka dari itu, Indonetwork.co.id hadir untuk memenuhi seluruh kebutuhan tersebut dalam skala nasional.</p>
			<h2>Realisasikan Potensi Usaha Anda Bersama Direktori Bisnis Kami</h2>
			<p>16 kategori utama, hampir 2 juta member yang terdaftar, dan lebih dari 4 juta pengunjung website per bulannya. Setelah 15 tahun berdiri, Indonetwork.co.id terus menjadi tujuan para wirausahawan untuk mempromosikan barang maupun jasa dagangannya. Kami yakin bahwa setiap UMKM berhak mendapatkan kesempatan yang sama untuk mengembangkan usahanya, terutama dari segi digital. Maka dari itu kami selalu berkomitmen dalam memberikan peluang sekaligus dukungan bagi UMKM di Indonesia melalui pemberdayaan secara digital, komunitas, dan bisnis.</p>
			<h2>Situs Pemasaran Jasa &amp; Barang Dagangan Terpercaya di Seluruh Indonesia</h2>
			<p>Peran penting kami selaku direktori bisnis online adalah mendorong dan membantu mengembangkan UMKM di Indonesia untuk Go Online sekaligus mengenalkan pendekataan baru terhadap pemasaran barang dagangan mereka. Indonetwork.co.id percaya bahwa setiap usaha memiliki keunikan tersendiri yang dapat dikembangkan melalui online platform kami yang telah tersedia. Pengalaman kami selama 15 tahun di bidang e-commerce menjadi keunggulan tersendiri untuk mengedukasi para penjual dalam menjalankan bisnis online mereka. Dengan jumlah pengunjung yang besar setiap harinya, sekarang Anda tidak perlu ragu lagi mencari platform untuk mempromosikan jasa dan barang dagangan Anda.
			<br><br>
			Indonetwork.co.id juga menyediakan pilihan kategori yang beragam mulai dari perangkat elektronik, peralatan industri, sampai produk kerajinan untuk memenuhi kebutuhan usaha dan pribadi Anda. Pembeli sekarang dapat dengan mudah mencari produk atau jasa yang diinginkan serta berkomunikasi langsung dengan penjual-penjual terpercaya yang sudah terdaftar.</p>
		</div>
	</div>
</div>
<?php include ("footer.php");?>