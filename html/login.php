<?php include ("head.php");?>
<?php include ("header.php");?>
<div class="static-box static-login">
	<form method="post" action="">
		<div class="form-box">
			<h2 class="blue-title">Login Anggota</h2>
			<hr size="1">
			<div class="signwith">
				<dl class="row">
					<dt class="col-md-4 col-sm-4 col-xs-4">Daftar dengan:</dt>
					<dd class="col-md-8 col-sm-8 col-xs-8">
						<a href=""><button class="btn btn-facebook" type="button"><img src="../bootstrap-sass/assets/images/fb-icon.png" alt="" width="20" height="20">&nbsp;&nbsp;Facebook</button></a>
						<a href=""><button class="btn btn-google" type="button"><img src="../bootstrap-sass/assets/images/google-icon.png" alt="" width="20" height="20">&nbsp;&nbsp;&nbsp; Google</button></a>
					</dd>
				</dl>
			</div>
			<hr size="1">
			<div class="loginwithform">
				<dl class="row">
					<dt class="col-md-4 col-sm-4 col-xs-4">E-mail</dt>
					<dd class="col-md-8 col-sm-8 col-xs-8"><input type="text" placeholder="E-mail/Username Anda" class="input-form"></dd>
					<dt class="col-md-4 col-sm-4 col-xs-4">Password</dt>
					<dd class="col-md-8 col-sm-8 col-xs-8"><input type="Password" placeholder="Password" class="input-form"></dd>
					<dt class="col-md-4 col-sm-4 col-xs-4">&nbsp;</dt>
					<dd class="col-md-8 col-sm-8 col-xs-8">
						<div class="alert alert-warning">
							E-mail / Username / Password tidak cocok
						</div>
					</dd>
				</dl>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 box-button">
						<input type="submit" value="Login" class="button">
					</div>
				</div>
			</div>
			<hr size="1">
			<div class="otherlink">
				<div class="row">
					<div class="col-md-5 col-sm-5 col-xs-5"><a href="">Kirim Ulang Kode Verifikasi</a></div>
					<div class="col-md-3 col-sm-3 col-xs-3 forgotpasslink"><a href="">Lupa Password?</a></div>
					<div class="col-md-4 col-sm-4 col-xs-4 newuserlink"><a href="">Daftar Anggota Baru</a></div>
				</div>
			</div>
		</div>
	</form>
</div>
<?php include ("footer.php");?>